package polimi.awt.resources;

import org.springframework.hateoas.ResourceSupport;
import polimi.awt.controller.UserController;
import polimi.awt.model.UserPV;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class UserPVResource extends ResourceSupport {

    private final UserPV userpv;

    public UserPVResource(final UserPV userpv) {
        this.userpv = userpv;
        final long id = userpv.getId();
        add(linkTo(UserController.class).withRel("users"));
    }
}
